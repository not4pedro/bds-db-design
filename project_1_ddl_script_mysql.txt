


-------------------------------------

	SCRIPT - MySQL

-------------------------------------


CREATE TABLE `customer` (
  `customer_id` SERIAL NOT NULL,
  `first_name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`customer_id`)
);

CREATE TABLE `delivery` (
  `delivery_id` SERIAL NOT NULL,
  `date` VARCHAR(20) NOT NULL,
  `delivery_type` VARCHAR(45) NOT NULL,
  `customer_id` SERIAL NOT NULL,
  PRIMARY KEY (`delivery_id`),

  
);

CREATE TABLE `category` (
  `category_id` SERIAL NOT NULL,
  `category_name` VARCHAR(20) NOT NULL,
  `category_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`category_id`)
);

CREATE TABLE `product` (
  `product_id` SERIAL NOT NULL,
  `product_name` VARCHAR(20) NOT NULL,
  `product_price` VARCHAR(20) NOT NULL,
  `stock` INTEGER NOT NULL,
  `order_id` SERIAL NOT NULL,
  `category_id` SERIAL NOT NULL,
  PRIMARY KEY (`product_id`),

    FOREIGN KEY (`category_id`)
      REFERENCES `category`(`category_id`)
);

CREATE TABLE `review` (
  `review_id` SERIAL,
  `comment` VARCHAR(45),
  `stars` INTEGER,
  `customer_id` SERIAL,
  `product_id` SERIAL,
  PRIMARY KEY (`review_id`),

    FOREIGN KEY (`product_id`)
      REFERENCES `product`(`product_id`),

    FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`)
);


CREATE TABLE `sale` (
  `sale_id` SERIAL NOT NULL,
  `sale_type` VARCHAR(45) NOT NULL,
  `customer_id` SERIAL NOT NULL,
  PRIMARY KEY (`sale_id`),

    FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`)
);

CREATE TABLE `address` (
  `address_id` SERIAL NOT NULL,
  `street_number` VARCHAR(45) NULL,
  `street_name` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `zipcode` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  PRIMARY KEY (`address_id`)


);


CREATE TABLE `cust_address` (
  `customer_id` SERIAL NOT NULL,
  `address_id` SERIAL NOT NULL,
  `address_type` VARCHAR(45) NOT NULL,

    FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`),

    FOREIGN KEY (`address_id`)
      REFERENCES `address`(`address_id`)
);


CREATE TABLE `card` (
  `card_id` SERIAL NOT NULL,
  `number` VARCHAR(16) NULL,
  `first_name` VARCHAR(20) NULL,
  `last_name` VARCHAR(20) NULL,
  `date` VARCHAR(20) NULL,
  `cvc` INTEGER NULL,
  `card_type` VARCHAR(45) NOT NULL,
  `customer_id` SERIAL NOT NULL,
  PRIMARY KEY (`card_id`)

  FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`)
);


CREATE TABLE `order` (
  `order_id` SERIAL NOT NULL,
  `date` VARCHAR(20) NOT NULL,
  `delivery_id` SERIAL NOT NULL,
  `customer_id` SERIAL NOT NULL,
  `product_id` SERIAL NOT NULL,
  `payment_id` SERIAL NOT NULL,
  PRIMARY KEY (`order_id`),

    FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`),
   
    FOREIGN KEY (`delivery_id`)
      REFERENCES `delivery`(`delivery_id`),  
   
    FOREIGN KEY (`payment_id`)
      REFERENCES `payment`(`payment_id`)
	
);

CREATE TABLE `shipping` (
  `shipping_id` SERIAL NOT NULL,
  `date` VARCHAR(20) NOT NULL,
  `order_id` SERIAL NOT NULL,
  PRIMARY KEY (`shipping_id`),
	
    FOREIGN KEY (`order_id`)
      REFERENCES `order`(`order_id`)
);

CREATE TABLE `payment` (
  `payment_id` SERIAL NOT NULL,
  `price` VARCHAR(20) NOT NULL,
  `date` VARCHAR(20) NOT NULL,
  `customer_id` SERIAL NOT NULL,
  PRIMARY KEY (`payment_id`),

    FOREIGN KEY (`customer_id`)
      REFERENCES `customer`(`customer_id`)
);


CREATE TABLE `product_order` (
  `product_id` SERIAL,
  `order_id` SERIAL,

    FOREIGN KEY (`product_id`)
      REFERENCES `product`(`product_id`),

    FOREIGN KEY (`order_id`)
      REFERENCES `order`(`order_id`)
);
